from datetime import datetime
from bson.objectid import ObjectId

from fastapi import File, UploadFile
from api.model.base_model import PyObjectId
from pydantic import BaseModel, EmailStr, Field

class User(BaseModel):
    id: PyObjectId = Field(default_factory = PyObjectId, alias = "_id")
    username: str = Field(..., max_length = 50)
    password: str = Field(...)
    name: str = Field(..., max_length = 100)
    email: EmailStr = Field(...)
    description: str = Field(None, max_length = 255)
    uiIdentityNumber: str = Field(..., max_length = 50)
    faculty: str = Field(..., max_length = 50)
    classOf: str = Field(..., max_length = 10)
    # profPic: UploadFile = File(None)
    linkedin: str = Field(None, max_length = 100)
    twitter: str = Field(None, max_length = 100)
    instagram: str = Field(None, max_length = 100)
    tiktok: str = Field(None, max_length = 100)
    communityEnrolled: list[str] = Field(None)
    eventEnrolled: list[str] = Field(None)
    joinedAt: datetime = Field(None)
    updatedAt: datetime = Field(None)

    class Config:
        json_encoders = { ObjectId: str }

class updateUser(BaseModel):
    name: str = Field(..., max_length = 100)
    description: str = Field(None, max_length = 255)
    faculty: str = Field(..., max_length = 50)
    classOf: str = Field(..., max_length = 10)
    #profPic: UploadFile = Field(None)
    linkedin: str = Field(None, max_length = 100)
    twitter: str = Field(None, max_length = 100)
    instagram: str = Field(None, max_length = 100)
    tiktok: str = Field(None, max_length = 100)
    updatedAt: datetime = Field(...)