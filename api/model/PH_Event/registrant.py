from datetime import datetime
from uuid import uuid4

from fastapi import FastAPI
from pydantic import BaseModel, EmailStr, Field

class Registrant(BaseModel):
  id: uuid4
  name: EmailStr = Field(...)
  eventId: str = Field(...)
  joinedAt: datetime = Field(...)