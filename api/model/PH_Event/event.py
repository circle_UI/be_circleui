from uuid import uuid4
from datetime import datetime

from fastapi import FastAPI, UploadFile
from pydantic import BaseModel, Field

class Event(BaseModel):
  id: uuid4
  name: str = Field(..., max_length = 50)
  status: str = Field(None, max_length = 10)
  description: str = Field(None, max_length = 255)
  dateTime: datetime = Field(...)
  location: str = Field(None, max_length = 30)
  contact: str = Field(None, max_length = 50)
  link: str = Field(None, max_length = 100)
  image: UploadFile = Field(None)
  price: str = Field(None, max_length = 10)
  organizer: str = Field(...)
  numberOfBookmark: int = Field(0, ge = 0)
  createdAt: datetime = Field(...)
  updatedAt: datetime = Field(...)