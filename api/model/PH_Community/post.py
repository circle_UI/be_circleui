from enum import Enum
from uuid import uuid4
from datetime import datetime

from fastapi import FastAPI
from pydantic import BaseModel, EmailStr, Field, UploadFile

class Post(BaseModel):
  id: uuid4
  title: str = Field(..., max_length = 50)
  content: str = Field(..., max_length = 255)
  link: str = Field(None, max_length = 255)
  attachment: UploadFile = Field(None)
  likesCounter: int = Field(0, ge = 0)
  creator: EmailStr = Field(...)
  communityId: str = Field(...)
  userLiked: list[EmailStr] = Field(None)
  createdAt: datetime = Field(...)
  updatedAt: datetime = Field(...)