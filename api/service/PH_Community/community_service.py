import json
from ...repository.repository import connect
from ...model.PH_Community.community_models import CommunityModel
from ...repository.PH_Community.community_repo import create_new_community, find_community_with_name, find_new_community, get_all_communities_from_db, get_certain_community_from_db, delete_one_community_from_db, get_community_with_name, update_community_from_db
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi import Body, HTTPException, status


async def create_community(community: CommunityModel = Body(...)):
    community = jsonable_encoder(community)
    name = community['name']
    # Check the uniqueness from the name of the group
    if(await find_community_with_name(name)) is not None:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=f"Community with the name '{name}' already exists")
    new_community = await create_new_community(community)
    created_community = await find_new_community(new_community)
    return created_community


async def get_all_communities():
    communities = await get_all_communities_from_db()
    return communities


async def get_certain_community(id: str):
    if(community := await get_certain_community_from_db(id)) is not None:
        return community
    raise HTTPException(status_code=404, detail=f"Community {id} not found")


async def update_community(user_email: str, id: str, data: dict):
    if(community := await get_certain_community_from_db(id)):
        if community['admin'] == user_email:
            update_result = await update_community_from_db(id, data)
            print(update_result.modified_count)

            if(updated_community := await get_certain_community_from_db(id)) is not None:
                return updated_community

            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"community {id} not found"
            )

        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail=f"community {id} not found"
            )

    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"community {id} not found"
        )


async def delete_community(id: str):
    delete_result = await delete_one_community_from_db(id)

    if delete_result.deleted_count == 1:
        return True
    else:
        return False


async def get_communities_by_name(name: str):
    if(communities := await get_community_with_name(name)) is not None:
        return communities


async def user_is_an_admin_of_community(user_email: str, community_id: str):
    community = await get_certain_community_from_db(community_id)
    if not community:
        return False
    if community['admin'] == user_email:
        return True
    else:
        return False
