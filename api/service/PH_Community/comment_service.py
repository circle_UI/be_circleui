from ...model.PH_Community.comment_models import CommentModel
from ...repository.PH_Community.comment_repo import create_new_comment, delete_certain_comment_from_db, find_comment, get_all_comments_from_db, get_certain_comment_from_db, update_comment_from_db
from fastapi.encoders import jsonable_encoder
from fastapi import Body, HTTPException, status
from pydantic import Field

async def create_comment(comment: CommentModel = Body(...), creator: str=Field(...)):
    comment = jsonable_encoder(comment)
    comment['creator'] = creator
    new_comment = await create_new_comment(comment)
    created_comment = await find_comment(new_comment)
    return created_comment

async def get_all_comments():
    comments = await get_all_comments_from_db()
    return comments

async def get_certain_comment(id:str):
    if(comment := await get_certain_comment_from_db(id)) is not None:
        return comment
    raise HTTPException(status_code=404, detail=f"Comment {id} not found")

async def update_comment(user_email: str, id:str, data:dict):
    if(comment := await get_certain_comment_from_db(id)):
        if comment['creator'] == user_email:
            await update_comment_from_db(id, data)
            if(updated_comment := await get_certain_comment_from_db(id)) is not None:
                return updated_comment
            
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"comment {id} not found"
            )
        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail=f"You're not the creator of this comment"
            )
    else:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND,
            detail=f"comment {id} not found"
        )

async def delete_comment(id:str):
    delete_result = await delete_certain_comment_from_db(id)

    if delete_result.deleted_count == 1:
        return True
    else:
        return False