from fastapi import APIRouter, Body, Security, status
from ...service.PH_Community.comment_service import *
from ...model.PH_Community.comment_models import *
from fastapi.responses import JSONResponse
from fastapi import Body, HTTPException, status
from api.model.PH_Main import user,token
from api.service.PH_Main import user as user_service

router = APIRouter(
    prefix='/api/v1',
    tags=['community'],
)

@router.post('/comment')
async def create_comment_controller(
    comment: CommentModel = Body(...),
    current_user: user.User = Security(
        user_service.get_current_active_user, scopes=['me']
        )
    ):
    content = await create_comment(comment, current_user['user']['email'])
    print(type(current_user['user']['email']))
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=content)

@router.get('/comment')
async def get_comment_controller(
    current_user: user.User = Security(
    user_service.get_current_active_user, scopes=['me'])
    ):
    comments = await get_all_comments()
    return comments

@router.get('/comment/{id}')
async def get_certain_comment_controller(
    id: str,
    current_user: user.User = Security(
    user_service.get_current_active_user, scopes=['me'])
    ):
    comment = await get_certain_comment(id)
    return comment

@router.put("/comment/{id}")
async def update_comment_controller(
    id: str,
    update_data: UpdateCommentModel = Body(...),
    current_user: user.User = Security(
        user_service.get_current_active_user, scopes=['me'])
    ):
    user_email = current_user['user']['email']
    updated_data = jsonable_encoder(update_data)
    comment = await update_comment(user_email, id, updated_data)
    if(comment is not None):
        return comment

@router.delete("/comment/{id}")
async def delete_community_controller(
        id: str,
    current_user: user.User = Security(
        # tinggal tambahin scope buat app admin
        user_service.get_current_active_user, scopes=['me'])
):
    delete_sucessfull = await delete_comment(id)
    if delete_sucessfull == True:
        return JSONResponse(status_code=status.HTTP_204_NO_CONTENT)
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND, detail=f"comment {id} not found")
