import certifi, os
import motor.motor_asyncio
from dotenv import load_dotenv

load_dotenv()
ca = certifi.where()

def connect():
    MONGODB_URL = os.environ['DEV_MONGODB_URL']
    DB_NAME = os.environ['DEV_DB_NAME']
    
    client = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URL, serverSelectionTimeoutMS = 5000, tlsCAFile = ca)

    try:
        return client[DB_NAME]
    except Exception:
        print("Unable to connect to the server")
        