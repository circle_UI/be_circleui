from ...model.PH_Community.comment_models import *
from ..repository import connect

db = connect()

async def create_new_comment(comment: CommentModel):
    return await db['comment'].insert_one(comment)

async def find_comment(new_comment):
    return await db['comment'].find_one({"_id":new_comment.inserted_id})

async def get_all_comments_from_db():
    return await db['comment'].find(skip=0, limit=0).to_list(1000)

async def get_certain_comment_from_db(id: str):
    result = await db['comment'].find_one({"_id":id})
    return result

async def update_comment_from_db(id: str, data:dict):
    update_result = await db['comment'].update_one({"_id":id},{"$set":data})
    return update_result

async def delete_certain_comment_from_db(id: str):
    delete_result = await db["comment"].delete_one({"_id":id})
    return delete_result