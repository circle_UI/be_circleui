from fastapi.testclient import TestClient
from app import app
from api.repository.repository import connect
from unittest import TestCase
from api.model.PH_Main.user import User
from fastapi.encoders import jsonable_encoder
from api.model.PH_Community.comment_models import *

client = TestClient(app)

db = connect()

class TestComment(TestCase):
    @classmethod
    def setUpClass(cls):
        # setUpClass: Run once to set up non-modified data for all class methods.
        cls.register = '/api/v1/register'
        cls.login = '/api/v1/login'
        cls.logout = '/api/v1/logout'
        cls.application_encoded = 'application/x-www-form-urlencoded'
        cls.application_json = 'application/json'
        cls.user = User(
            username="test_comment",
            password="pplasikgaboong",
            name="Admin Community PPL",
            email="test_comment@ui.ac.id",
            description="Lyfe is never flat as a community admin",
            uiIdentityNumber="1906900999",
            faculty="faculty of Psychology",
            classOf="2010",
            linkedin="linkedin.com/admin_budackPPL",
            twitter="twitter.com/admin_budackPPL",
            instagram="instagram.com/admin_budackPPL",
            tiktok="tiktok.com/admin_budackPPL",
            communityEnrolled=[
                "string"
            ],
            eventEnrolled=[
                "string"
            ]
        )
        client.post(
            url=cls.register,
            json=jsonable_encoder(cls.user)
        )
        cls.profile = db['user'].find_one(
            {'username': 'test_comment'})
    
    @classmethod
    def tearDownClass(cls):
        # tearDownClass: Run once to clean up after all class methods has been executed.
        db['user'].delete_one({'username': 'test_comment'})



    def setUp(self):
        # setUp: Run once for every test method to setup clean data.
        response = client.post(
            self.login,
            headers={
                'Content-Type':self.application_encoded
            },
            data={
                'username': 'test_comment',
                'password': 'pplasikgaboong',
                'scope': 'me users logout app-admin'
            }
        )
        self.token = response.json()['access_token']
        # Create Comment
        self.comment =  CommentModel(
            content= "TEST",
        )
        client.post(
            "/api/v1/comment",
            json=jsonable_encoder(self.comment),
            headers={
            'accept': self.application_json,
            'Authorization': f"Bearer {self.token}"
            }
        )
        self.data1 = jsonable_encoder(self.comment)

    def tearDown(self):
        # tearDown: Clean up run after every test method.
        client.get(
            url=self.logout,
            headers={
                'accept': self.application_json,
                'Authorization': f"Bearer {self.token}"
            }
        )
        db['comment'].delete_many({"creator": "test_comment@ui.ac.id"})
    
    def test_create_comment_invalid(self):
        response = client.post(
            "/api/v1/comment",
            json={
                
            },
            headers={
                'accept': self.application_json,
                'Authorization': f"Bearer {self.token}"
            }
        )
        assert response.status_code == 422
        assert response.json() == {
            "detail": [
                {
                    "loc": [
                        "body",
                        "content"
                    ],
                    "msg": "field required",
                    "type": "value_error.missing"
                }
            ]
        }

    
    def test_create_comment_valid(self):
        response = client.post(
            "/api/v1/comment",
            json={
                "content":"TEST"
            },
            headers={
                'accept': self.application_json,
                'Authorization': f"Bearer {self.token}"
            }
        )
        assert response.status_code == 201
        assert response.json()['creator'] == "test_comment@ui.ac.id"


    def test_update_comment(self):
        response = client.put(
            f"/api/v1/comment/{jsonable_encoder(self.comment)['_id']}",
            json={
                "content":"GG"
            }, headers={
                'accept': self.application_json,
                'Authorization': f"Bearer {self.token}"
            },
        )
        assert response.status_code == 200
        assert response.json()['creator'] == "test_comment@ui.ac.id"
    
    def test_update_comment_invalid(self):
        response = client.put(
            f"/api/v1/comment/1",
            json={
                "content":"AZZ"
            }, headers={
                'accept': self.application_json,
                'Authorization': f"Bearer {self.token}"
            }
        )
        assert response.status_code == 404
        assert response.json()['detail'] == "comment 1 not found"

    def get_comment_test(self):
        response = client.get(
            "/api/v1/comment",
            headers={
            'accept': self.application_json,
            'Authorization': f"Bearer {self.token}"
            }
        )
        assert response.status_code == 200
        assert self.data1 in response.json()

    def test_get_certain_comment_invalid(self):
        response = client.get(
            f"/api/v1/comment/1",
            headers={
            'accept': self.application_json,
            'Authorization': f"Bearer {self.token}"
            }
        )
        assert response.status_code == 404
        assert response.json()['detail'] == "Comment 1 not found"
    
    def test_get_certain_comment_valid(self):
        response = client.get(
            f"/api/v1/comment/{jsonable_encoder(self.comment)['_id']}",
            headers={
            'accept': self.application_json,
            'Authorization': f"Bearer {self.token}"
            }
        )
        assert response.status_code == 200
        assert response.json()['creator'] == "test_comment@ui.ac.id"

# TODO : bikin test buat delete