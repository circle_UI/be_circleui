from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from api.controller.PH_Main import (
  user,
)

from api.controller.PH_Community import (
  community_controller,
  comment_controller
)

app = FastAPI(title = 'Circle UI')
app.include_router(user.router)
app.include_router(community_controller.router)
app.include_router(comment_controller.router)

origins = [
   "*"
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)